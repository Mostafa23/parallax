'use strict';

const firstPage = document.querySelector('.firstpage');
const firstPageH1 = document.querySelector('.firstpage h1');
const background = document.querySelector('.background');
const middleground = document.querySelector('.middleground');
const foreground = document.querySelector('.foreground');

const secondPage = document.querySelector('.secondpage');
const secondPageSpan1 = secondPage.querySelectorAll('h1 span')[0];
const secondPageSpan2 = secondPage.querySelectorAll('h1 span')[1];
const secondPageSpan3 = secondPage.querySelectorAll('h1 span')[2];
const secondPageImg = secondPage.querySelector('img');

const thirdPage = document.querySelector('.thirdpage');



const FIRST_SCROLL = 500;
const SECOND_SCROLL = 700;
const SECOND_SCROLL_2 = 900;



document.addEventListener('scroll', function (event) {
    let scrollOffset = window.scrollY;
    let windowHeight = document.documentElement.clientHeight;

    if (scrollOffset <= FIRST_SCROLL) {
        firstPage.style.opacity = 1 - (scrollOffset / FIRST_SCROLL);
        firstPageH1.style.transform = `scale(${1 + (scrollOffset /FIRST_SCROLL)})`;
        foreground.style.transform = `scale(${1 + 0.7 * (scrollOffset /FIRST_SCROLL)})`;
        middleground.style.transform = `scale(${1 + 0.2 * (scrollOffset /FIRST_SCROLL)})`;
        background.style.transform = `scale(${1 + 0.3 * (scrollOffset /FIRST_SCROLL)})`;
        secondPageSpan1.style.transform = `translate(0,0)`;
        secondPageSpan2.style.transform = `translate(0,0)`;
        secondPageSpan3.style.transform = `translate(0,0)`;
        secondPageSpan1.style.opacity = 1;
        secondPageSpan2.style.opacity = 1;
        secondPageSpan3.style.opacity = 1;
    }

    if (scrollOffset <= FIRST_SCROLL) {
        firstPage.style.display = 'block';
    } else {
        firstPage.style.display = 'none';
    }

    if (scrollOffset > FIRST_SCROLL && scrollOffset <= SECOND_SCROLL) {
        secondPageSpan1.style.transform = `translate(0,${FIRST_SCROLL - scrollOffset}px)`;
        secondPageSpan2.style.transform = `translate(${(FIRST_SCROLL - scrollOffset)}px,0)`;
        secondPageSpan3.style.transform = `translate(0,${(scrollOffset - FIRST_SCROLL)}px)`;
        if (scrollOffset > ((FIRST_SCROLL + SECOND_SCROLL) / 2)) {
            secondPageSpan1.style.opacity = (1 - (scrollOffset / SECOND_SCROLL)) * 6.5;
            secondPageSpan2.style.opacity = (1 - (scrollOffset / SECOND_SCROLL)) * 6.5;
            secondPageSpan3.style.opacity = (1 - (scrollOffset / SECOND_SCROLL)) * 6.5;
        }
    }

    if (scrollOffset <= SECOND_SCROLL) {
        secondPageImg.style.display = 'none';
    } else {
        secondPageImg.style.display = 'block';
    }


    if (scrollOffset > SECOND_SCROLL && scrollOffset <= SECOND_SCROLL_2) {
        secondPageSpan1.style.opacity = 0;
        secondPageSpan2.style.opacity = 0;
        secondPageSpan3.style.opacity = 0;
        secondPageImg.style.transform = `scale(${4 * (scrollOffset - SECOND_SCROLL)/SECOND_SCROLL_2})`;
    }


    if(scrollOffset > SECOND_SCROLL_2){
        let yEffectScroll = scrollOffset - SECOND_SCROLL_2;
        secondPage.style.transform = `translateY(${-yEffectScroll}px)`;
        thirdPage.style.transform = `translateY(${windowHeight - yEffectScroll}px)`;
    }
    else{
        secondPage.style.transform = '';
    }

});
